package pe.metro.www.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;


public class ProductoPage {
    public static final Target TXT_PRODUCTO = Target.the("producto").locatedBy("//a[contains(text(),'{0}')]");
    public static final Target BTN_AGREGAR_CARRITO = Target.the("botón agregar al carrito").locatedBy("//span[text()='Agregar al carrito']");
    public static final Target BTN_CARRITO = Target.the("botón carrito").locatedBy("//button[@class='btn red minicart__action--toggle-open food-site']");
    public static final Target BTN_VER_CARRITO =Target.the("botón ver carrito").locatedBy("//span[contains(text(),'Ver carrito')]");
}
