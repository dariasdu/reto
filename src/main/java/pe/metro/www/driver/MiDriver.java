package pe.metro.www.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MiDriver {

    static WebDriver driver;

    public static MiDriver web(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\didie\\Downloads\\RetoSophos\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        return new MiDriver();
    }

    public WebDriver enLaPagina(String url){
        driver.get(url);
        return driver;
    }
}
