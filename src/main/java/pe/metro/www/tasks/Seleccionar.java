package pe.metro.www.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import pe.metro.www.models.DatosProductos;

import java.util.List;

import static pe.metro.www.userinterfaces.ProductoPage.*;

public class Seleccionar implements Task {

    private String producto;

    public Seleccionar(List<DatosProductos> datosProductos) {
        this.producto = datosProductos.get(0).getProducto();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        /*Se realizan las diferentes acciones sobre los targets.
        1. Selecciona el producto
        2. Lo agrega al carrito
        3. Va al carrito
         */
        actor.attemptsTo(Click.on(TXT_PRODUCTO.of(producto)),
                Click.on(BTN_AGREGAR_CARRITO),
                Click.on(BTN_CARRITO),
                Click.on(BTN_VER_CARRITO));
    }
    public static Seleccionar elProducto(List<DatosProductos> datosProductos) {
        return Tasks.instrumented(Seleccionar.class, datosProductos);
    }
}
